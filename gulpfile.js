var gulp = require('gulp'),
		compass = require('gulp-compass'),
  	path = require('path'),
  	livereload = require('gulp-livereload'),
  	serve = require('gulp-serve');

gulp.task('compass', function() {
  gulp.src('./sass/*.scss')
    .pipe(compass({
      css: 'css',
      sass: 'sass'
    }))
    .pipe(gulp.dest('./public/css'))
    .pipe(livereload({ auto: false }));
});

gulp.task('serve', serve('public'));

gulp.task('watch', ['serve'], function() {
  livereload.listen();
  gulp.watch('sass/**', ['compass']);
  gulp.watch('public/**').on('change', livereload.changed);
});